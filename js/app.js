var factoriesInfo = (typeof customFactoriesInfo === "object" ) ? customFactoriesInfo : {
  welcomeIcons: ["heart.png","hat.png"],
  factories:  [
    {
      "name": "Laravel Framework",
      "shortName": "Laravel",
      "description": "The PHP Framework For Web Artisans",
      "bgImage": "//i.imgur.com/8i9g0rW.png",
      "slideImage": "//i.imgur.com/4EJzx9m.png",
      "url": "https://laravel.themefactory.net",
      "view_cnt": 0,
      "download_cnt": 0
    },
    {
      "name": "Yii Framework",
      "shortName": "Yii",
      "description":"<p>Yii is a high-performance PHP framework best for developing Web 2.0 applications.</p><p>Yii comes with rich features: MVC, DAO/ActiveRecord, I18N/L10N, caching, authentication and role-based access control, scaffolding, testing, etc.</p>",
      "bgImage":"//i.imgur.com/TASLjrF.jpg",
      "slideImage":"//i.imgur.com/5OQOZWm.png",
      "url": "https://yii.themefactory.net",
      "view_cnt": 0,
      "download_cnt": 0
    },
    {
      "name": "WordPress",
      "shortName": "WP",
      "description":"<p>WordPress is web software you can use to create a beautiful website or blog. We like to say that WordPress is both free and priceless at the same time.</p>",
      "bgImage": "//i.imgur.com/hbWb763.jpg",
      "slideImage":"//i.imgur.com/Klwsb6G.png",
      "url": "https://wordpress.themefactory.net",
      "view_cnt": 0,
      "download_cnt": 0
    },
    {
      "name": "Yii Framework 2",
      "shortName": "Yii2",
      "description":"<p>Yii 2 is a complete re-write on top of PHP 5.4.0+ and it will not be compatible with 1.1. The templates are not interchangeable, so you must use Yii2 templates for Yii2 apps.</p>",
      "bgImage":"//i.imgur.com/7qBHL9s.jpg",
      "slideImage":"//i.imgur.com/F3y5kNt.png",
      "url": "https://yii2.themefactory.net",
      "view_cnt": 0,
      "download_cnt": 0
    },
    {
      "name": "Twitter Bootstrap",
      "shortName": "Bootstrap",
      "description":"Sleek, intuitive, and powerful mobile first front-end framework for faster and easier web development.",
      "bgImage":"//i.imgur.com/leka2d9.jpg",
      "slideImage":"//i.imgur.com/OPnlQgy.png",
      "url": "https://bootstrap.themefactory.net",
      "view_cnt": 0,
      "download_cnt": 0
    },
    {
      "name": "HTML",
      "shortName": "HTML",
      "description":"HTML is a standardized system for tagging text files to achieve font, color, graphic, and hyperlink effects on World Wide Web pages.",
      "bgImage":"//i.imgur.com/AoxPbpo.jpg",
      "slideImage":"//i.imgur.com/E7IbqS9.png",
      "url": "https://html.themefactory.net",
      "view_cnt": 0,
      "download_cnt": 0
    },
    {
      "name": "Icon",
      "shortName": "Icon",
      "description":"Icon Fonts are Awesome because you can easily change the size, color, shadow etc. You can do all the other stuff image based icons can do, like change opacity or rotate or whatever.",
      "bgImage":"//i.imgur.com/i02ICtT.jpg",
      "slideImage":"//i.imgur.com/FHhCmE5.png",
      "url": "https://icon.themefactory.net",
      "view_cnt": 0,
      "download_cnt": 0
    },
    {
      "name": "Sewing Pattern",
      "shortName": "Pattern",
      "description":"<p>A random variety of useful (and not that useful) FREE sewing patterns for every occassions.</p>",
      "bgImage": "//i.imgur.com/dNoVI3I.jpg",
      "slideImage":"//i.imgur.com/zxYE6Q9.png",
      "url": "https://pattern.themefactory.net",
      "view_cnt": 0,
      "download_cnt": 0
    },
    {
      "name": "Email",
      "shortName": "Email",
      "description":"<p>A large selection of FREE email templates useful for newsletters and mailing-lists.</p>",
      "bgImage": "//i.imgur.com/soc3Dm4.jpg",
      "slideImage":"//i.imgur.com/m8oSLU9.png",
      "url": "https://email.themefactory.net",
      "view_cnt": 0,
      "download_cnt": 0
    }

  ]
};

var webApp = angular.module("webApp",[]);

webApp.controller("AppController", function($scope){
  if( typeof factoriesInfo != "undefined" && typeof factoriesInfo.factories != "undefined" ) {
    $scope.factories = factoriesInfo.factories
  } else {
    $scope.factories = [];
  }

  $scope.welcomeIcon = factoriesInfo.welcomeIcons[Math.floor(Math.random()*factoriesInfo.welcomeIcons.length)];
});

webApp.directive("factoryDirective", function(){
  return{
    restrict: "C",
    replace: true,
    scope: {
      ngModel: "="
    },
    template: $("#factory-template").html()
  }
});
